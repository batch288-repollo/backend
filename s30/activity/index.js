async function fruitsOnSale(db) {
	return await(

		db.fruits.aggregate([
				{$match: {onSale: true}},
				{$count: "Fruits on sale"}
			])

		);
};


async function fruitsInStock(db) {
	return await(

		db.fruits.aggregate([
				{$match: {stock: {$gte: 20}}},
				{$count: "With stocks more than or equal to 20"}
			])

		);
};


async function fruitsAvePrice(db) {
	return await(

		db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group: {
					_id: "$supplier_id",
					average_price: {$avg: "$price"}
				}}
			])

		);
};


async function fruitsHighPrice(db) {
	return await(

		db.fruits.aggregate([
				{$group: {
					_id: "$supplier_id",
					highest_price: {$max: "$price"}
				}}
			])

		);
};


async function fruitsLowPrice(db) {
	return await(

		db.fruits.aggregate([
				{$group: {
					_id: "$supplier_id",
					highest_price: {$min: "$price"}
				}}
			])

		);
};


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
