// console.log("hey");

function printInput() {
	let nickname = "Chris!";
	console.log("Hi " + nickname);
}
printInput();

// For other cases, functions can also process data directly passed into it instead of relying only on Global Variables.
// Consider this function:
function printName(name) {
	console.log("My name is " + name);
}
printName("Juana!");
printName("Arya!");
printName(); // Undefined but will not throw error.

// Variables can also be passed as an argument.
let sampleVariable = "Curry";
let sampleArray = ["Davis", "Green", "Jokic", "Tatum"];
printName(sampleVariable);
printName(sampleArray); // Forced coercion happens here.

// One example of using the Parameter and Argument.
function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + "divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);

// Functions as Argument
// Function parameters can also accept other functions as arguments.

function argumentFunction() {
	console.log("This function was passed as an argument before the message was printed!");
}

function invokeFunction(func) {
	func();
}

invokeFunction(argumentFunction);

// Function as Argument with return keyword.
function returnFunction() {
	let name = "Chris";
	return name;
}

function invokedFunction(func) {
	console.log(func());
}

invokedFunction(returnFunction);

// Using Multiple Arguments/parameters:
// Multiple arguments will correspond to the number of parameters declared in a function in succeeding order.
function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}
createFullName("Juan", "Dela", "Cruz");

// In JavaScript, providing more or less arguments than the expected will not return an error.
// Providing less argument than the expected parameters will automatically assign an undefined value to the parameter.
createFullName("Juan", "Cruz");
createFullName("Juan", "Cruz", null);
// Not so strict language.
// In other programming languages, this will return an error stating that the number of arguments do not match the number of parameters.
createFullName("Juan", "Cruz", 'Crazy', 'Lola');

// Using variable as Multiple arguments
// let firstName = "John";
// let middleName = "Doe";
// let lastName = "Smith";
let firstName = "John", middleName = "Doe", lastName = "Smith"; // Can declare multiple var's. in one line.
createFullName(firstName, middleName, lastName);

// Using alert()
// alert() allows us to show a small window at the top of our browser page to show information to our users.
// As opposed to console.log which only shows the message on the console.
// Syntax: alert("messageInString");
// alert("Hello World!"); // This will run immediately when the page loads.

function showSampleAlert() {
	alert("Hello, user!");
}
// showSampleAlert();
console.log("I will only log in the console when the alert is dismissed.");

// Using prompt()
// prompt() allows us to show a small window at the top of the browser to gather user input.
// Syntax: prompt("dialogInString");
// Returned data type is "String". The value gathered from a prompt is returned as a string.

// let samplePrompt = prompt("Enter your name.");
// console.log("Hello, " + samplePrompt);
// console.log(typeof samplePrompt);

// let age = prompt("Enter your age.");
// console.log(age);
// console.log(typeof age);

// Returned null if prompt was cancelled.
// Returns an empty string when there is no input or null if the user cancels the prompt.

// let sampleNullPrompt = prompt("Don't enter anything.");
// console.log(sampleNullPrompt);

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter your last name.");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}
printWelcomeMessage();
