const express = require("express");
const taskControllers = require("../controllers/taskControllers.js");

// Contains all the endpoints of our application.
const router = express.Router();

router.get("/", taskControllers.getAllTasks);
router.post("/addTask", taskControllers.addTasks);

// Parameterizer:
// We are going to create a route using a Delete method at the url "/tasks/:id".
// The colon (:) here is an identifier that helps to create a dynamic route which allows us to supply information.
router.delete("/:id", taskControllers.deleteTask);

module.exports = router;
