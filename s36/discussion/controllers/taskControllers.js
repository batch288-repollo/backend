const Task = require('../models/task.js');

// Controllers:
// This controller will get/retrieve all the document from the tasks collection.
module.exports.getAllTasks = (request, response) => {
	Task.find({}).then(result => {
		return response.send(result);
	}).catch(error => {
		return response.send(error);
	});
}

// Create a controller that will add data in our database.
module.exports.addTasks = (request, response) => {
	Task.findOne({name: request.body.name}).then(result => {
		if (result !== null) {
			return response.send("Duplicate Task.");
		}
		else {
			let newTask = new Task({
				name: request.body.name
			});
			newTask.save();
			return response.send("New task created!");
		}
	}).catch(error => response.send(error));
}

module.exports.deleteTask = (request, response) => {
	let taskToBeDeleted = request.params.id;
	console.log(taskToBeDeleted);	
	// In mongoose, we have the findByIdAndRemove method that will look for a document with
	// the same id provided from the URL and remove/delete the document in MongoDB.
	Task.findByIdAndRemove(taskToBeDeleted).then(result => {
		return response.send(`The document that has the _id of ${taskToBeDeleted} has been deleted.`);
	}).catch(error => response.send(error));
}
