const express = require("express");
const taskControllers = require("../controllers/taskControllers.js");

const router = express.Router();

router.get("/", taskControllers.getAllTasks);
router.get("/:id", taskControllers.getSpecificTask);
router.post("/addTask", taskControllers.addTasks);
router.put("/:id/complete", taskControllers.updateTaskStatus);
router.delete("/:id", taskControllers.deleteTask);

module.exports = router;
