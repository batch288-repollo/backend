const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routers/taskRoutes.js");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin@batch288repollo.jojjyon.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});
mongoose.connection.on("error", console.error.bind(console, "Error! Can't connect to the db."));
mongoose.connection.once("open", () => console.log('We are now connected to the db!'));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/tasks", taskRoutes);

if (require.main = module) {
	app.listen(port, () => console.log(`The server is running at port ${port}.`))
}
module.exports = app;
