const Task = require("../models/taskModels.js");

module.exports.getAllTasks = (request, response) => {
	Task.find({}).then(result => {
		return response.send(result);
	}).catch(error => {
		return response.send(error);
	});
}

module.exports.getSpecificTask = (request, response) => {
	Task.findOne({_id: request.params.id}).then(result => {
		if (result !== null) {
			return response.send(result);
		}
		else {
			return response.send("No task found.");
		}
	}).catch(error => response.send(error));
}

module.exports.addTasks = (request, response) => {
	Task.findOne({name: request.body.name}).then(result => {
		if (result !== null) {
			return response.send("Duplicate Task.");
		}
		else {
			let newTask = new Task({
				name: request.body.name
			});
			newTask.save();
			return response.send("New task created!");
		}
	}).catch(error => response.send(error));
}

module.exports.updateTaskStatus = (request, response) => {
	let taskToBeCompleted = request.params.id;

	Task.updateOne({_id: taskToBeCompleted}, {status: 'complete'}).then(result => {
		return response.send(`The task that has an _id of ${taskToBeCompleted} has been completed.`);
	}).catch(error => response.send(error));
}

module.exports.deleteTask = (request, response) => {
	let taskToBeDeleted = request.params.id;
	console.log(taskToBeDeleted);

	Task.findByIdAndRemove(taskToBeDeleted).then(result => {
		return response.send(`The document that has the _id of ${taskToBeDeleted} has been deleted.`);
	}).catch(error => response.send(error));
}
