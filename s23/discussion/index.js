// console.log("rrr");

// [Section] Objects
/*
	- an object is a data type that is used to represent real world objects.
	- it is a collection of related data and/or functionalities.
	Structure of Objects:
		- key	- is what we call the property of an object.
		- value - is the value of the specific key or property.
*/
// Creating using the object initializers / object literal notation.
/*
	Syntax:
	let objectName = {
		keyA: valueA,
		keyB: valueB,
		...
	}
*/
let cellphone = {
	// key-value pairs
	name: 'Nokia 3210',
	manufactureDate: 1999
}
console.log('Result from creating objects using initializers/literal notation:');
console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.length);

let exampleArray = [1, 2, 3, 4, 5];
console.log(exampleArray);
console.log(typeof exampleArray);
console.log(exampleArray.length);

// Creating objects using constructor function (class constructor).
// Creates a reusable function to create several objects that have the same data structure.
// This is useful for creating multiple instances / copies of an object.
// An instance is a concrete occurance which emphasizes on the distinct / unique identity of it.
/*
	Syntax:
	function objectName(valueA, valueB) {
		this.keyA = valueA;
		this.keyB = valueB;
	}
*/
// The "this" keyword allows us to assign a new object's properties by associating them with the values 
// received from a constructor's function parameters.
function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// This is a unique instance of the Laptop object.
/*
	- the "new" operator creates an instance of an object.
	- Objects and instances are often interchanged because of object literals (let object = {})
	and instances (let object = new object) are distinct / unique objects.
*/
let laptop = new Laptop('lenovo', 2008);
console.log('Result from creating objects using the object constructors:');
console.log(laptop);

// This is another unique instance of the Laptop object.
let myLaptop = new Laptop('Macbook Air', 2020);
console.log('Result from creating objects using the object constructors:');
console.log(myLaptop);

// This invokes/calls the "Laptop" function instead of creating a new object instance.
// Returns "undefined" without the "new" operator because the "Laptop" function does not have a return statement.
let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log('Result from creating objects using the object constructors:');
console.log(oldLaptop);

// Creating empty objects:
let computer = {};
let myComputer = new Object();


// [Section] Accessing Object Properties
// Using the dot(.) notation:
console.log('--------------------------------------------------------------------------------------------------------');
console.log('Result from dot notation: ' + myLaptop.name);

// Using square bracket notation:
console.log('Result from square bracket notation: ' + myLaptop['name'] + ', ' + myLaptop['manufactureDate']);

// Accessing array objects:
// Accessing array elements can also be done using square brackets.
// Accessing object properties using the square bracket notation and array indexes can cause confusion.
// By using dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects.
// Object properties have names that make it easier to associate pieces of information.
let array = [laptop, myLaptop];

// May be confused for accessing array indexes.
console.log('laptop: ' + array[0]['name']);
// Differentiate between accessing arrays and object properties.
// This tells us that array[0] is an object by using dot notation.
console.log('myLaptop: ' + array[1].name);


// [Section] Initializing / Adding / Deleting / Reassigning Object Properties
/*
	- Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared.
	- This is useful for times when an object's properties are undetermined at the time of creating them. 
*/
console.log('--------------------------------------------------------------------------------------------------------');
let car = {};

// Initializing/adding object properties using dot notation.
car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation:');
console.log(car);

// Initializing/adding object properties using square bracket notation.
// While using the square bracket it will allow access to spaces when assigning property names to make it easier to read.
// This also makes it so that object properties can only be accessed using the square bracket notation.
// This also make names of object properties to not follow commonly used naming conventions for them.
car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']); // Undefined (must be EXACT CASING).
console.log(car.manufactureDate); // Undefined (must be EXACT CASING).
console.log('Result from adding properties using square bracket notation:');
console.log(car);

// Deleting object properties:
delete car['manufacture date'];
console.log('Result from deleting properties:');
console.log(car);

// Re-assigning object properties:
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties:');
console.log(car);


// [Section] Object Methods
/*
	- A method is a function which is a property of an object.
	- They are also functions and one of the key differences they have is that methods are functions related to a specific object.
	- Methods are useful for creating object specific functions which are used to perform tasks on them.
	- Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work.
*/
console.log('--------------------------------------------------------------------------------------------------------');
let person = {
	name: 'John',
	talk: function() {
		console.log('Hello my name is ' + this.name);
		// return "Hello my name is " + this.name;
	}
}
console.log(person);
console.log('Result from object methods:');
person.talk();

// Adding methods to objects.
person.walk = function() {
	console.log(this.name + ' walked 25 steps forward.');
}
person.walk();

// Methods are useful for creating reusable functions that perform tasks related to objects.
console.log('--------------------------------------------------------------------------------------------------------');
let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		state: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@mail.xyz'],
	introduce: function() {
		let fullName = this.firstName + ' ' + this.lastName + '.';
		console.log('Hello my name is ' + fullName);
	}
};
friend.introduce();

// [Section] Real world application of objects.
/*
	Scenario:
	1. We would like to create a game that would have several pokemon interact with each other.
	2. Every pokemon would have the same set of stats, properties and functions.
*/
console.log('--------------------------------------------------------------------------------------------------------');
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled target pokemon");
		console.log("Target pokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}
console.log(myPokemon);

// Creating an object constructor will help in this process.
function Pokemon(name, level) {
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	}
	this.faint = function() {
		console.log(this.name + ' fainted!');
	}
}

// Create new instance of "Pokemon" object each with their unique properties.
let pikachu = new Pokemon('Pikachu', 16);
let rattata = new Pokemon('Rattata', 8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects.
pikachu.tackle(rattata);

