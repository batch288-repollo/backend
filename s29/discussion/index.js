// Advanced Queries:
// Query an embedded document / object.
db.users.find({
	contact : {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
});

db.users.find({
	contact : {
		email: "stephenhawking@gmail.com"
	}
});

// Dot notation: (must use JSON format)
db.users.find({
	"contact.email" : "stephenhawking@gmail.com"
});

// Querying an array with exact element.
db.users.find({
	courses: ["CSS", "Javascript", "Python"]
});

db.users.find({
	courses: ["React"]
});

// All operator: (Does not need to be in exact order.)
db.users.find({
	courses: {
		$all: ["React", "Sass"]
	}
});


// Query Operators
// [SECTION] Comparison Query Operators
// $gt / $gte operator:
/*
	- allows us to find documents that have field number values greater than or equal to a specified value.
	- Note that this operator will only work if the data type of the field is number or integer.
	Syntax:
	db.collectionName.find({field: {
		$gt: value
	}});
	db.collectionName.find({field: {
		$gte: value
	}});
*/
db.users.find({age: {$gt: 76}});
db.users.find({age: {$gte: 76}});

// $lt / $lte operator:
/*
	- allows us to find documents that have field number less than or equal to a specified value.
	- Same with $gt / $gte operator, this will only work if the data type of the field being queried is a number or integer.
	Syntax:
	db.collectionName.find({field: {$lt: value}});
	db.collectionName.find({field: {$lte: value}});
*/
db.users.find({age: {$lt: 76}});
db.users.find({age: {$lte: 76}});

// $ne operator:
/*
	- allows us to find documents that have field number values not equal to specified value.
	Syntax:
	db.collectionName.find({field: {$ne: value}});
*/
db.users.find({age: {$ne: 76}});

// $in operator:
/*
	- allows us to find documents with specific match criteria of at least one field using different value.
	Syntax:
	db.collectionName.find({field: {$in: [value1, value2, ...]]}});
*/
db.users.find({lastName: {$in: ["Hawking", "Doe", "Armstrong"]}});
db.users.find({"contact.phone" : {$in: ["87654321"]}});

// Using $in operator in an array.
db.users.find({courses: {$in: ["React"]}});
db.users.find({courses: {$in: ["React", "Laravel"]}});


// [Section] Logical Query Operators
// $or operator:
/*
	- allow us to find documents that match a single criteria from multiple provided search criteria.
	Syntax:
		db.collectionName.find({
			$or: [{fieldA: valueA}, {fieldB: valueB}]
		})
*/
db.users.find({
	$or: [
		{firstName: "Neil"},
		{age: 25}
	]
});

// Add multiple opertors:
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 25}}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 25}}]});
db.users.find({
	$or:
	[
		{firstName: "Neil"},
		{age: {$gt: 25}},
		{courses: {$in: ["CSS"]}}
	]
});

// $and operator:
/*
	- allows us to find documents matching all the multiple criteria in a single field.
	Syntax:
	db.collectionName.find({
		$and:
		[
			{fieldA: valueA},
			{fieldB: valueB},
			...
		]
	});
*/
// Finding range of values using AND.
db.users.find({
	$and:
	[
		{age: {$lt: 80}},
		{age: {$gt: 25}}
	]
});
db.users.find({
	$and:
	[
		{age: {$ne: 82}},
		{age: {$ne: 76}}
	]
});

// Mini-activity:
// Query all documents from users collection that follow these criteria.
/*
	1. Information of the documents older than 30 that is enrolled in CSS or HTML.
*/
db.users.find({
	$and:
	[
		{age: {$lt: 30}},
		{courses: {$in: ["CSS", "HTML"]}}
	]
});


// [SECTION] Field Projection
/*
	- retrieving documents are common operations that we do and by default mongoDB queries return 
	the whole document as a response.
*/
// Inclusion
/*
	- allows us to include or add specific fields only when retrieving documents:
	Syntax:
	db.collectionName.find({criteria}, {field: 1});
*/
db.users.find({firstName: "Jane"}, {firstName: 1, lastName: 1, contact: 1});
db.users.find({firstName: "Jane"}, {firstName: 1, lastName: 1, contact: 1, _id: 0});
// dot notation in JSON:
db.users.find({firstName: "Jane"}, {firstName: 1, lastName: 1, "contact.phone": 1, _id: 0});

// Find All but suppressed.
db.users.find({}, {firstName: 1, lastName: 1, _id: 0});


// Exclusion
/*
	- allows us to exclude / remove specific fields only when retrieving documents.
	Syntax:
	db.collectionName.find({criteria}, {field: 0});
*/
db.users.find({firstName: "Jane"}, {contact: 0, department: 0});
db.users.find({firstName: "Jane"}, {"contact.email": 0, department: 0});

db.users.find(
{
	$or:
	[
		{firstName: "Jane"},
		{age: {$lte: 30}}
	]
}, 
{
	contact: 0, 
	department: 0
});

// slice code:
db.users.find(
{
	$or:
	[
		{firstName: "Jane"},
		{age: {$lte: 30}}
	]
}, 
{
	contact: 0, 
	department: 0
	courses: {$slice: 1}
});

// Evaluation Query Operator:
// $regex operator
/*
	- allow us to find documents that match a specific string pattern using regular expressions.
	Syntax:
	db.users.find({
		field: {$regex: 'pattern', options: 'optionValue'}
	});
*/
db.users.find({firstName: {$regex: 'N'}});
db.users.find({firstName: {$regex: 'n'}});
db.users.find({firstName: {$regex: 'en'}});
db.users.find({firstName: {$regex: 'n', $options: 'i'}});

