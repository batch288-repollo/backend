const port = 3001;
const express = require("express");
const mongoose = require("mongoose");
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin:admin@batch288repollo.jojjyon.mongodb.net/registration?retryWrites=true&w=majority", {useNewUrlParser: true});
mongoose.connection.on('error', console.error.bind(console, "Error! Can't connect to the database."));
mongoose.connection.once("open", () => console.log("We are connected to the cloud database!"));

// DB Schema and Model: User
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});
const User = mongoose.model('User', userSchema);

// Routing:
app.post("/signup", (request, response) => {
	let reqUsername = request.body.username;
	let reqPassword = request.body.password;

	User.findOne({username: reqUsername}).then(result => {
		if (result != null) {
			return response.send("Duplicate username found!");
		}
		else if (reqUsername !== '' && reqPassword !== '') {
			let newUser = new User({
				username: reqUsername,
				password: reqPassword
			});
			try {
				newUser.save();
				return response.status(201).send('New user registered.');
			}
			catch (error) {
				console.error(error);
			}
		}
		else {
			return response.send("BOTH username and password must be provided.");
		}
	});
});

app.get("/showUsers", (request, response) => {
	User.find({}).then(result => {
		return response.send(result);
	}).catch(error => response.send(error))
});



if (require.main === module) {
	app.listen(port, () => console.log(`Server is running at port ${port}.`));
}
module.exports = app;
