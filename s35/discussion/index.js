const port = 3001;
// Mongoose is a package that allows creation of Schemas to model our data structures.
// Also has access to a number of methods for manipulating our database.
const mongoose = require("mongoose");
const express = require("express");
const app = express();

// Middlewares
// Allows our app to read json data.
app.use(express.json());
// Allows our app to read data from any url data type.
app.use(express.urlencoded({extended: true}));
// Listen to port 3001.

// [Section]: MongoDB Connection:
/*
	- Connect to the database by passing your connection string.
	- Due to update in Mongo DB drivers that allow connection, the default connection string is being flagged as an error.
	- By default a warning will be displayed in the terminal when the application is running.
	- {newUrlParser: true}
	Syntax:
	mongoose.connect("MongoDB string", {useNewUrlParser: true});
*/
mongoose.connect("mongodb+srv://admin:admin@batch288repollo.jojjyon.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});
// Notification whether we connected properly with the database.
let db = mongoose.connection;

// For catching the error just in case we had an error during the connection.
// console.error.bind allows us to print errors in the browser and in the terminal.
db.on('error', console.error.bind(console, "Error! Can't connect to the database."));
// If the connection was successful.
db.once("open", () => console.log("We are connected to the cloud database!"));


// [Section] Mongoose Schemas:
// Schemas are the ones that determine the structure of the document to be written in the database.
// Schemas act as blueprint to our data.
// Use the Schema() constructor of the mongoose module to create a new Schema object.
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type.
	name: String,
	// let us another field which is status.
	status: {
		type: String,
		default: "pending"
	},
});

// [Section] Models:
// Uses schema and are used to create / instantiate objects that corresponds to the schema.
// Models use Schema and they act as the middleman from the server (JS code) to our database.
// To create a model we are going to use the model() method.
// Use Capital first letter for model() variable. Variable name will be the collection name.
// This creates a tasks collection in the database. 
// The singular variable name will become plural and all small letters in the MongoDB database.
// Automatically detects if plural already.
const Task = mongoose.model('Task', taskSchema);

// [Section] Routes:
// Create a POST route to create a new task.
// Create a new task.
/*
	Business Logic:
	1. Add a functionality to check whether there are duplicate tasks.
	2. If the task is existing in the database, we return an error.
	3. If the task doesn't exist in the database, we add it in the database.
	4. The task data will be coming from the request's body.
*/
app.post("/tasks", (request, response) => {
	// findOne() method is a mongoose method that acts similar to "find" in MongoDB.
	// If the findOne() method finds a document that matches the criteria, it will return the object/document and if
	// there's no object that matches the criteria it will return an empty object or null.
	Task.findOne({name: request.body.name}).then(result => {
		if (result != null) {
			return response.send("Duplicate task found!");
		}
		else {
			// Create a new task and save it to the database.
			let newTask = new Task({
				name: request.body.name
			});
			// The save() method will store the information to the database.
			// Since the newTask was created from the Mongoose Schema and Task Model, it will be saved in the tasks collection.
			newTask.save();
			return response.send('New task created!');
		}
	});
});

/*
	Get all the tasks in our collection.
	1. Retrieve all the documents.
	2. If an error is encountered, print the error.
	3. If no error/s is/are found, send a success status to the client and show the documents retrieved.
*/
app.get("/tasks", (request, response) => {
	// find() method is a mongoose method that is similar to MongoDB find.
	Task.find({}).then(result => {
		return response.send(result);
	}).catch(error => response.send(error))
});


if (require.main === module) {
	app.listen(port, () => console.log(`Server is running at port ${port}.`));
}
module.exports = app;

// "npm install mongoose" - installs the mongoose library.

