const port = 3000;
const express = require("express");
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

let users = [];

// Start of Routing:
app.get("/home", (request, response) => {
	response.send('Welcome to the home page.');
});

app.get("/users", (request, response) => {
	response.send(users);
});

app.post("/signup", (request, response) => {
	let i;
	for (i = 0; i < users.length; i++) {
		if (request.body.username == users[i].username) {
			response.send(`Username already exist.`);
			break;
		}
	}
	if (i >= users.length) {
		if (request.body.username !== '' && request.body.password !== '') {
			users.push(request.body);
			response.send(`User ${request.body.username} successfully registered!`);
		}
		else {
			response.send(`Please input both username and passowrd.`);
		}
	}
});

app.put("/change-password", (request, response) => {
	let message = 'User does not exists!';
	for (let i = 0; i < users.length; i++) {
		if (request.body.username == users[i].username) {
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated!`;
			break;
		}
	}
	response.send(message);
});

app.delete("/delete-user", (request, response) => {
	let message = 'User does not exist.';
	if (users.length == 0) {
		message	= 'No users found.';
	}

	for (let index = 0; index < users.length; index++) {
		if (request.body.username == users[index].username) {
			users.splice(index, 1);
			message = `User ${request.body.username} has been deleted.`;
			break;
		}
	}

	response.send(message);
});


if (require.main === module) {
	app.listen(port, () => console.log(`Server running at port ${port}.`));
}
module.exports = app;
