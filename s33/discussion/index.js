// console.log('---------------------------------------------------------------------------------------------');

// [Section] JavaScript Synchronous vs Asynchronous
// JavaScript by default is synchronous meaning that only one statement is executed at a time.
// This can be proven when a statement has an error, javascript will not proceed with the next statement.
// console.log("Hello World!");
// conole.log("Hello World!");
// console.log("Hello World!");

// When certain statements take a lot of time to process, this slows down our code.
// for (let i = 0; i <= 100000; i++) {
// 	console.log(i);
// }
// console.log('Hello again!');

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.


// [Section] Getting all posts
// The Fetch API allows you to asynchronously request for a resource data.
// So it means the fetch method that we are going to use here will run asynchronously.
/*
	Syntax:
	fetch('URL');
*/
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's 
// resulting value.
// console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

/*
	Syntax:
	fetch('URL').then((response => response));
*/
// The fetch method will return a promise that resolves the response object.
// The "then" method captures the response object that can be used in a function paramter.
// fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response));

// Use the JSON method from the response object to convert the data retrieved into JSON format to be used in our application.
// fetch('https://jsonplaceholder.typicode.com/posts').then(response => {console.log(response.json())});

fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(json => console.log(json));
console.log('---------------------------------------------------------------------------------------------');
// console.log(fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()));

// The "async" and "await" keyword, it is another approach that can be used to achieve asynchronous code.
// Creates an asynchronous function.
// The "async" and "await" keyword must be used together.
async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);
	let json = await result.json();
	console.log(json);
}
// fetchData();


// [Section] Getting specific post.
// Retrieves specific post following the rest API(/posts/:id)
fetch('https://jsonplaceholder.typicode.com/posts/5').then(response => response.json()).then(json => console.log(json));

// [Section] Creating Post
/*
	- options is an object that contains the method, the header and the body of the request.
	- By default if you don't add a specific method in the fetch request, it will be a GET method.
	Syntax:
	fetch('URL', options)
	.then(response => {})
	.then(response => {})
*/
fetch('https://jsonplaceholder.typicode.com/posts', {
	// sets the method of the request object to post following the REST API.
	method: 'POST',
	// sets the header data of the request object to be sent to the backend.
	// specified that the content will be in JSON structure.
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		userId: 1
	})
}).then(response => response.json()).then(json => console.log(json));


// [Section] Update a specific Post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		// id: 1,
		title: 'Updated Post'
		// body: 'Hello again!',
		// userId: 1
	})
}).then(response => response.json()).then(json => console.log(json));

// PATCH
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		title: 'Updated Post'
	})
}).then(response => response.json()).then(json => console.log(json));

// The PUT method is a method of modifying resource where the client sends data that updates the entire object/document.
// while PATCH applies a partial update to the (resources) object/document.


// [Section] DELETE a Post
// Deleting a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {method: "DELETE"})
.then(response => response.json()).then(json => console.log(json));

// "npm init / npm init -y" (initialize JSON package)
