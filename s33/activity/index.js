//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.
// console.log('aaaaaa');
// Get Single To Do [Sample]
async function getSingleToDo() {

    return await (

       //add fetch here.

       fetch('<urlSample>')
       .then((response) => response.json())
       .then((json) => json)

   );

}



// Getting all to do list item
getAllToDo();
async function getAllToDo() {

   return await (
      fetch("https://jsonplaceholder.typicode.com/todos")
      .then(response => response.json())
      .then(json => {
         console.log(json.map(key => key.title))
      })
   );

}

// [Section] Getting a specific to do list item
getSpecificToDo();
async function getSpecificToDo() {

   return await (
      fetch("https://jsonplaceholder.typicode.com/todos/1")
      .then(response => response.json())
      .then(json => console.log(json))
   );

}

// [Section] Creating a to do list item using POST method
createToDo();
async function createToDo() {
   
   return await (
      fetch("https://jsonplaceholder.typicode.com/todos/", {
         method: 'POST',
         headers: {'Content-Type': 'application/json'},
         body: JSON.stringify({
            title: 'New post',
            completed: true,
            userId: 0
         })
      })
      .then(response => response.json())
      .then(json => console.log(json))
   );

}

// [Section] Updating a to do list item using PUT method
updateToDo();
async function updateToDo() {

   return await (
      fetch("https://jsonplaceholder.typicode.com/todos/1", {
         method: 'PUT',
         headers: {'Content-Type': 'application/json'},
         body: JSON.stringify({
            title: 'Updated To Do List Item',
            description: 'Description',
            status: 'Pending',
            dateCompleted: 'Pending',
            userId: 1
         })
      })
      .then(response => response.json())
      .then(json => console.log(json))
   );

}

// [Section] Deleting a to do list item
deleteToDo();
async function deleteToDo() {

   return await (
      fetch('https://jsonplaceholder.typicode.com/todos/1', {method: "DELETE"})
      .then(response => response.json())
      .then(json => console.log(json))
   );

}



//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err) {

}
