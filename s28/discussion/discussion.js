// show databases - list of the db inside out cluster.
// use 'dbName' - to use specific database.
// show collections - to see the list of collections inside the db.

// CRUD operation
/*
	- CRUD operation is the heart of any backend application.
	- mastering the CRUD operation is essential for any developer especially to those who want to become backend developers.
*/

// [SECTION] Inserting Document (Create)
// Insert one document
/*
	- Since mongoDB deals with objects as it's structure for documents, 
	we can easily create them by providing objects in our method/operation.

	Syntax:
		db.collectionName.insertOne({
			object
		})
*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});

// Insert Many
/*
	Syntax:
	db.collectionName.insertMany([{objectA}, {objectB}]);
*/
db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},

		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "876543210",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	]);


// [SECTION] Finding documents (Read operation).
// db.collectionName.find();
// db.collectionName.find({field:value});

// Using the find() method will show the list of all documents inside our collection.
// The "pretty" method allows us to be able to view the documents returned by or the terminals to be in a better format.
db.users.find();
db.users.find().pretty();

// Using the find({field:value}) method will return the documents that will pass the criteria given in the method.
db.users.find({firstName: "Stephen"});
db.users.find({firstName: "Jane"});

// Finding inside Object properties.
db.users.find({contact: {phone: "87654321", email: "stephenhawking@gmail.com"}});
db.users.find({"contact.phone" : "87654321"});

// 646c595584e927351f53fb39
db.users.find({_id: ObjectId("646c595584e927351f53fb39")});

// Multiple criteria's:
db.users.find({lastName: "Armstrong", age: 82});

// Only returns one document.
db.rooms.findOne({name: "double"});


// [SECTION] Updating documents (Update)
db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test.gmail.com"
	},
	courses: [],
	department: "none"
});

// updateOne method:
/*
	Syntax:
	db.collectionName.updateOne(
		{criteria},
		{
			$set: {
				{field:value}
			}
		}
	)
*/
db.users.updateOne(
		{firstName: "test"},
		{
			$set: {
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "12345678",
					email: "bill@gmail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "none"
			}	
		}
	);

db.users.updateOne(
		{firstName: "Bill"},
		{
			$set: {
				firstName: "Ronald"
			}
		}
	);

db.users.updateOne(
		{firstName: "Jane"},
		{
			$set: {
				lastName: "Edited"
			}
		}
	);

// Updating multiple documents
/*
	Syntax:
	db.collectionName.updateMany(
		{criteria},
		{
			$set: {
				{field:value}
			}
		}
	)
*/
db.users.updateMany(
		{department: "none"},
		{
			$set: {
				department: "HR"
			}
		}
	);

// Replace One
/*
	Syntax: db.collectionName.replaceOne(
		{criteria}, {
			$set: {
				object
			}
		}
	)
*/
db.users.insert({firstName: "test"});
db.users.replaceOne(
		{firstName: "test"},
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {},
			courses: [],
			department: "Operations"
		}
	);


// [SECTION] Deleting documents.
/*
	db.collectionName.deleteOne({criteria});
*/
db.users.deleteOne({firstName: "Bill"});

// Deleting multiple documents.
/*
	db.collectionName.deleteMany({criteria});
*/
db.users.deleteMany({firstName: "Jane"});

// All documents in our collection will be deleted.
db.users.deleteMany({});

