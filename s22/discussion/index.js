// console.log('rrr');

// Array Methods:
// JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// [Section] Mutator Methods
// Mutator methods are functions that "mutate" or change an array after they're created.
// These methods manipulate the original array performing various things such as adding or removing elements.

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push() method:
/*
	- Adds an element in the end of an array and returns the updated array's length.
	Syntax:
	arrayName.push();
*/
console.log("Current array:");
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log("Mutated array from push method:");
console.log(fruits);
console.log(fruitsLength);

// Pushing multiple elements to an array.
fruitsLength = fruits.push('Avocado', 'Guava');
console.log("Mutated array from pushing multiple elements:")
console.log(fruits);
console.log(fruitsLength);

// pop() method:
/*
	- removes the last element and returns the removed element.
	Syntax:
	arrayName.pop();
*/
console.log('------------------------------------------------------------------------------------------');
console.log("Current array:");
console.log(fruits);

let removedFruit = fruits.pop();
console.log("Mutated array from pop method:");
console.log(fruits);
console.log(removedFruit);

// unshift() method:
/*
	- it adds one or more elements at the beginning of an array AND it returns the updated array's length.
	Syntax:
	arrayName.unshift('elementA');
	arrayName.unshift('elementA', 'elementB', 'elementC', ..., ...);
*/
console.log('------------------------------------------------------------------------------------------');
console.log("Current array:");
console.log(fruits);

fruitsLength = fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method:");
console.log(fruits);
console.log(fruitsLength);

// shift() method:
/*
	- removes an element at the beginning of an array AND returns the removed element.
	Syntax:
	arrayName.shift();
*/
console.log('------------------------------------------------------------------------------------------');
console.log("Current array:");
console.log(fruits);

removedFruit = fruits.shift();
console.log("Mutated array from shift method:");
console.log(fruits);
console.log(removedFruit);

// splice() method:
/*
	- Simultaneously remove elements from a specified index number and adds element.
	Syntax:
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/
console.log('------------------------------------------------------------------------------------------');
console.log("Current array:");
console.log(fruits);

// fruits.splice(4, 2, 'Lime', 'Cherry'); // Adds 2 and deletes 2.
// fruits.splice(4, 2, 'Lime'); // Add only 1 and deletes 2.
// fruits.splice(5, 0, 'Cherry'); // Add only 1 and doesn't delete.
fruits.splice(fruits.length, 0, "Cherry");
console.log("Mutated array after the splice method:");
console.log(fruits);

// sort() method:
/*
	Rearranges the array elements in alphanumeric order.
	Syntax:
	arrayName.sort()
*/
console.log('------------------------------------------------------------------------------------------');
console.log("Current array:");
console.log(fruits);

fruits.sort();
console.log("Mutated array after the sort method:");
console.log(fruits);

// reverse() method:
/*
	- reverses the order of array elements.
	Syntax:
	arrayName.reverse();
*/
console.log('------------------------------------------------------------------------------------------');
console.log("Current array:");
console.log(fruits);

fruits.reverse();
console.log("Mutated array after the reverse method:");
console.log(fruits);


// [Section] Non-Mutator Methods
/*
	- Non-mutator methods are functions that do not modify or change an array after they're created.
	- These methods do not manipulate the original array performing various task such as returning elements from an array
		and combining arrays and printing the output.
*/
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf() method: - left to right
// returns the index number of the first matching element found in an array.
// If no match was found, the result will be -1.
// The search process will be done from the first element proceeding to the last element.
/*
	Syntax:
	arrayName.indexOf(searchValue);
	arrayName.indexOf(searchValue, startingIndex);
*/
console.log('------------------------------------------------------------------------------------------');
let firstIndex = countries.indexOf('PH');
console.log(firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log(invalidCountry);

firstIndex = countries.indexOf('PH', 2);
console.log(firstIndex);

console.log(countries);

// lastIndexOf() - right to left
/*
	- returns the index number of the last matching element found in an array.
	- the search process will be done from the last element proceeding to the first element.
	Syntax:
	arrayName.lastIndexOf(searchValue);
	arrayName.lastIndexOf(searchValue, startingIndex)
*/
let lastIndex = countries.lastIndexOf('PH');
console.log(lastIndex);

invalidCountry = countries.lastIndexOf('BR');
console.log(invalidCountry);

lastIndex = countries.lastIndexOf('PH', 4);
console.log(lastIndex);

console.log(countries);

// slice()
/*
	- portions/slices elements from an array AND return a new array.
	Syntax:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, endingIndex);
*/
// Slicing off elements from a specified index to the last element.
console.log('------------------------------------------------------------------------------------------');
let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index.
// The elements that will be sliced are elements from the starting element until the element before the ending index in the argument.
let slicedArrayB = countries.slice(2, 7);
console.log("Result from slice method (start - end):");
console.log(slicedArrayB);

// slicing off elements starting from the last element of an array.
let slicedArrayC = countries.slice(-5);
console.log("Result from slice method (end - start):");
console.log(slicedArrayC);

// slicedArrayD = countries.slice(-5, 4);
// console.log("Result from slice method (end - start):");
// console.log(slicedArrayD);


// toString();
/*
	returns an array as string separated by commas.
	Syntax:
	arrayName.toString();
*/
let stringArray = countries.toString();
console.log("Result from toString() method:");
console.log(stringArray);
console.log(typeof stringArray);

// concat()
/*
	- combines arrays to an array or elements and returns the combined result.
	Syntax:
	arrayA.concat(arrayB);
	arrayA.concat(elementA);
*/
console.log('------------------------------------------------------------------------------------------');
let tasksArrayA = ["drink HTML", "eat JavaScript"];
let tasksArrayB = ["inhale CSS", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log(tasks);

let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log(combinedTasks);

let allTasks = tasksArrayA.concat(tasksArrayB.concat(tasksArrayC));
console.log(allTasks);

let exampleTasks = tasksArrayA.concat(tasksArrayB.concat('smell express'));
console.log(exampleTasks);

// join();
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));


// [Section] (Iterators) Iteration Methods
// Iteration methods are loops designed to perform repetitive task.
// Iteration methods loops over all items in an array.

// forEach()
// Similar to for loop that iterates on each of array elements.
/*
	Syntax:
	arrayName.forEach(function(indivElement){};);
*/
console.log('------------------------------------------------------------------------------------------');
console.log(allTasks);
allTasks.forEach(function(task) {
	console.log(task);
});

// filteredTask variable will hold all the elements from the allTasks array that has more than 10 characters.
let filteredTask = [];
let i = 0;
allTasks.forEach(function(task) {
	if (task.length > 10) filteredTask.push(task);
});
console.log(filteredTask);

// map()
// Same as forEach() but
// Iterates on each element and RETURNS NEW ARRAY with different values depending on the
// result of the function's operation.
let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number) {
	return number*number;
});
console.log(numbers);
console.log(numberMap);

// every()
/*
	- It will check if all elements in an array meet the given condition.
	- return true value if all elements meet the condition and false otherwise.
	Syntax:
	let/const resultArray = arrayName.every(function(indivElement) {
		return expression/condition;
	});
*/
numbers = [1, 2, 3, 4, 5];
let areAllValid = numbers.every(function(number) {
	return (number < 3);
});
console.log(areAllValid);

// some();
// checks if at least one element in an array meets the given condition.
/*
	Syntax:
	let/const resultArray = arrayName.some(function(indivElement) {
		return expression/condition;
	});
*/

let areSomeValid = numbers.some(function(number) {
	return (number < 2);
});
console.log(areSomeValid);

// filter();
// returns new array that contains elements which meets the given condition.
// returns an empty array if no elements were found.
/*
	Syntax:
	let/const resultArray = arrayName.filter(function(indivElement) {
		return expression/condition;
	});
*/
numbers = [1, 2, 3, 4, 5];
let filterValid = numbers.filter(function(number) {
	return (number % 2 === 0);
});
console.log(filterValid);

// includes();
// checks if the argument passed can be found in the array.
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let isProductFound = products.includes('Mouse'); // Case-Sensitive
console.log(isProductFound);

// reduce();
// evaluates elements from left to right and returns or reduces the array into a single value.
numbers = [1, 2, 3, 4, 5];
// The first parameter in the function will be the accumulator. Accumulate the results and re-operates with the current value.
// The second parameter in the function will be the current Value.
let reducedArray = numbers.reduce(function(x, y) {
	console.log('Accumulator: ' + x);
	console.log('Current Value: ' + y);
	return x + y;
})
console.log(reducedArray);

products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
reducedArray = products.reduce(function(x, y) {
	return x + y;
});
console.log(reducedArray);
