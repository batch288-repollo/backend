// console.log("hello");

// [Section] While Loop
// A while loop takes an expression/condition.
// Expressions are any unit of code that can be evaluated as true or false. 
// If the condition evaluates to be true, the statement / code block will be executed.
// A loop will iterate a certain number of times until an expression/condition is true.
// Iteration is the term given to repitition of statements.
/*
	Syntax:
	while(expression/condition) {
		statement;
		increment/decrement;
	}
*/
let count = 5;
while(count !== 0) {
	// Current value of count printed.
	console.log("While : " + count);
	// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
	// Forgetting to include this in our loop will make our 
	// applications run an infinite loop which will eventually crash our device.
	// After running the script, if a slow response from the browser is experienced or
	// an infinite loop is seen in the console quickly close the application/browser/tab to avoid this.
	count--;
}

// [Section] Do While Loop
// A do while loop works a lot like the while loop.
// But unlike while loops, do-while-loop guarantees that the code will be executed at least once.
/*
	Syntax:
	do {
		statement;
		incr/decr;
	} while (expression/condition)
*/

// The number() function works similarly as the parseInt() function.
// let number = Number(prompt("Give me a number:"));
// do {
// 	console.log("Do While: " + number);
// 	// Increment
// 	number++;
// } while (number < 10)


// [Section] For Loop
/*
	A for loop is more flexible than while and do-while loops.
	It consists of 3 parts:
	1. The "initialization" value that will track the progression of the loop.
	2. expression/condition that will be evaluated which will determine whether the loop will
		rune one more time.
	3. The "iteration" w/c indicates how advance the loop.
	
	Syntax:
	for(initialization; expression/condition; iteration) {
		statement;
	}
*/
console.log("=================================================");
// Will create a loope that starts at 0 and ends with 20.
for (let count = 0; count <= 20; count += 2) {
	console.log("The current value of count is " + count);
}

console.log("=================================================");
let myString = "alexis";
// .length property. Characters in strings can be counted using this property.
console.log(myString.length);

// Access elements of a string / the characters of the string.
console.log(myString[0]);
console.log(myString.charAt(3));
// Since the last index in myString is 3, therefore index 5 will output undefined.
console.log(myString[5]);

// We will create a loop that will print out the individual letters of myString variable;
console.log("=================================================");
for (let index = 0; index < myString.length; index++) {
	console.log(myString.charAt(index));
	// console.log(myString[index]);
}

// Create a string named "myName" w/ value of Alex;
console.log("=================================================");
let myName = "Alex";
/*
	Create a loop that will print out the letter of the name individually
	and printout the number 3 instead when the character is a vowel.
*/
for (let index = 0; index < myName.length; index++) {
	let lowerCasedChar = myName[index].toLowerCase();
	let isVowel = lowerCasedChar === 'a' || lowerCasedChar === 'e' || lowerCasedChar === 'i' ||
					lowerCasedChar === 'o' || lowerCasedChar === 'u';
	
	// if (lowerCasedChar === 'a' || 
	// 	lowerCasedChar === 'e' ||
	// 	lowerCasedChar === 'i' ||
	// 	lowerCasedChar === 'o' ||
	// 	lowerCasedChar === 'u') {

	// 	console.log(3);
	// }

	if (isVowel) {
		console.log(3);
	}
	else {
		console.log(myName[index]);	
	}	
}

// replace()
console.log("=================================================");
let replaceString = "YoYo Yeye"
replaceString = replaceString.replace("Yeye", "Yaya");
console.log(replaceString);


// [Section] Continue and Break Statements
// The "continue" statement allows the code to go to the next iteration of the loop without
// finishing the execution of all statements in a code block.
// The "break" statement is used to terminate the current loop once a match has been found.

// Create a loop that if the count value is divided by 2 and the remainder is 0,
// it will print the number and continue to the next iteration of the loop.
for (let count = 0; count <= 20; count++) {
	if(count >= 10) {
		console.log("The number is equal to 10, end loop!");
		break;
	}

	if (count % 2 === 0) {
		console.log("The number is divisible by 2, skip!");
		continue;
	}
	console.log(count);
}

// Create a loop that will iterate based on the length of the string name.
// We are going to console the letters per line, if the letter is equal to 'a'
// we are going to console "Continue to the next iteration" then add continue() statement;
// If current letter is equal to 'd', stop the loop.
console.log("=================================================");
let name = "alexandro";
for (let index = 0; index < name.length; index++) {
	let charLowerCased = name[index].toLowerCase();
	if (charLowerCased === 'a') {
		console.log("Continue to the next iteration!");
		continue;
	}
	if (charLowerCased === 'd') {
		break;
	}
	console.log(name[index]);
}

