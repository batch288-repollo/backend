const User = require("../models/Users.js");
const Course = require("../models/Courses.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

// Controllers:
// Create a controller for signup.
// registerUser():
/*
	Business Logic / Flow:
	1. First, validate whether the user is existing or not by checking whether the email exist on our databases or not.
	2. If the user email is existing, we will prompt an error telling the user that the email is taken.
	3. Otherwise, we will signup or add user in our database.
*/
module.exports.registerUser = (request, response) => {
	// find() method: it will return an array of object that fits the given criteria.
	User.findOne({email: request.body.email}).then(result => {
		// Truthy value, if has value then = true, else null = false.
		if (result) {
			return response.send(false);
		}
		else {
			new User({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				// hashSync() method hash/encrypt our password. 2nd argument - salt rounds or encryption iterations.
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo
			}).save().then(saved => {
				return response.send(true);
			}).catch(error => response.send(false));
		}
	}).catch(error => response.send(false));
}

module.exports.loginUser = (request, response) => {
	User.findOne({email: request.body.email}).then(result => {
		if (result) {
			// compareSync() method is used to compare a non-encrypted password from the login form 
			// to the encrypted password retrieved from the find method (MongoDB). 
			// It returns true or false depending on the result of comparison.
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			if (isPasswordCorrect) {
				return response.send({auth: auth.createAccessToken(result)});
			}
			else {
				return response.send(false);
			}
		}
		else {
			return response.send(false);
		}
	}).catch(error => response.send(error));
}

module.exports.getProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	// console.log(userData);
	if (userData.isAdmin) {
		User.findById(request.body.id).then(result => {
			if (result) {
				result.password = '';
				return response.send(result);
			}
			else {
				return response.send(`User ID ${request.body.id} is not yet registered!`);
			}
		}).catch(error => response.send(error));
	}
	else {
		return response.send("You are not an admin, you don't have access to this route.");
	}
}

// Controller for enroll course:
module.exports.enrollCourse = async (request, response) => {
	const courseId = request.body.courseId;
	const userData = auth.decode(request.headers.authorization);
	let isAlreadyEnrolledCourse = false;
	let isAlreadyEnrolledUser = false;
	let isCourseInactive = false;
	let i = 0;

	if (userData.isAdmin) {
		return response.send(false);
	}
	else {
		// Push update to "users" document array objects.
		let isUserUpdated = await User.findOne({_id: userData.id}).then(result => {			
			for (i = 0; i < result.enrollments.length; i++) {
				// console.log(result.enrollments[i].courseId);
				if (result.enrollments[i].courseId == courseId) {
					isAlreadyEnrolledCourse = true;
					return false;
				}
			}
			if (i === result.enrollments.length) {
				result.enrollments.push({
					courseId: courseId
				});
				return result.save().then(saved => true).catch(error => {
					console.log(error);
					return false;
				});
			}
		}).catch(error => {
			console.log(error);
			return false;
		});
		// Push update to "courses" document array objects.
		let isCourseUpdated = await Course.findOne({_id: courseId}).then(result => {
			if (result.isActive == false) {
				isCourseInactive = true;
				return false;
			}

			for (i = 0; i < result.enrollees.length; i++) {
				// console.log(result.enrollees[i].userId)
				if (result.enrollees[i].userId == userData.id) {
					isAlreadyEnrolledUser = true;
					return false;
				}
			}

			if (i === result.enrollees.length) {
				result.enrollees.push({
					userId: userData.id
				});
				return result.save().then(saved => true).catch(error => {
					console.log(error);
					return false;
				});
			}
		}).catch(error => {
			console.log(error);
			return false;
		});

		// If condition to check whether we updated users and courses document successfully.
		if (isAlreadyEnrolledCourse && isAlreadyEnrolledUser) {
			return response.send(false);
		}
		else if (isUserUpdated && isCourseUpdated) {
			return response.send(true);
		}
		else if (isCourseInactive) {
			return response.send(false);
		}
		else {
			return response.send(false);
		}
	}
}

module.exports.getUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	User.findOne({_id: userData.id})
	.then(result => response.send(result))
	.catch(error => response.send(false));
}
