const Course = require("../models/Courses.js");
const auth = require("../auth.js");

// This controller is for the course creation.
module.exports.addCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		new Course({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive,
			slots: request.body.slots
		}).save().then(saved => response.send(true))
		.catch(error => response.send(false));
	}
	else {
		return response.send(false);
	}
}

// In this controller, we are going to retrieve all of the courses in our database.
module.exports.getallCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		Course.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false));
	}
	else {
		return response.send(false);
	}
}

// Route for retrieving all active courses.
module.exports.getActiveCourses = (request, response) => {
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

// Controller will retrieve the information of a single document using the provided params.
module.exports.getCourse = (request, response) => {
	Course.findById(request.params.courseId)
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

// This controller will update our document or course.
module.exports.updateCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		const courseId = request.params.courseId;
		let updatedCourse = {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price
		}
		Course.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else {
		return response.send(false);
	}
}

module.exports.archiveCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		const courseId = request.params.courseId;
		const updatedCourse = {
			isActive: request.body.isActive
		}
		Course.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	}
	else {
		return response.send(false);
	}
}

// Controller for retrieving archived courses.
module.exports.getInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(false));
	}
}
