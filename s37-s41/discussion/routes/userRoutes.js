const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");
const express = require("express");
const router = express.Router();

// Routes:
router.post("/register", userControllers.registerUser);
router.post("/details", auth.verify, userControllers.getProfile);
router.post("/login", userControllers.loginUser);
router.post("/enroll", auth.verify, userControllers.enrollCourse);
router.get("/userDetails", auth.verify, userControllers.getUserDetails);

// Token Verification:
// router.get("/verify", auth.verify);

module.exports = router;
