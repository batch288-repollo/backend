const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");
const express = require("express");

const router = express.Router();

// courseControllers Routes:
router.post("/addCourse", auth.verify, courseControllers.addCourse);
// Route for retrieving all courses.
router.get("/", auth.verify, courseControllers.getallCourses);
// Route for retrieving all active courses.
router.get("/activeCourses", courseControllers.getActiveCourses);
//Route for inactive courses.
router.get("/inactiveCourses", auth.verify, courseControllers.getInactiveCourses);
// Route for getting a specific course information.
router.get("/:courseId", courseControllers.getCourse);
// Route for updating course.
router.patch("/:courseId", auth.verify, courseControllers.updateCourse);
// Route for disabling course.
router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourse);

module.exports = router;
