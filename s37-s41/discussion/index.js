const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");
const express = require("express");
const mongoose = require("mongoose");
// It will allow our backend application to be available to our frontend application.
// It will also allows us to control the app's Cross Origin Resource Sharing settings.
const cors = require("cors");

const port = 4001;
const app = express();

// MongoDB connection:
// Establish the connection between the DB and the application / server.
// The name of the database should be "CourseBookingAPI".
mongoose.connect("mongodb+srv://admin:admin@batch288repollo.jojjyon.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.on("error", console.error.bind(console, "Error! Can't connect to the database."));
mongoose.connection.once("open", () => console.log("We are connected to the database."));

// Reminder that we are going to use this for the sake of the bootcamp.
app.use(cors()); // Because this line will allow all domain to access our backend.
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

if (require.main === module) {
	app.listen(port, () => console.log(`The server is now running at port ${port}.`));
}
module.exports = {app,mongoose};
