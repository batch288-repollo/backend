const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");

const port = 4001;
const app = express();

mongoose.connect("mongodb+srv://admin:admin@batch288repollo.jojjyon.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.on("error", console.error.bind(console, "Error! Can't connect to the database."));
mongoose.connection.once("open", () => console.log("We are connected to the database."));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(port, () => console.log(`The server is now running at port ${port}.`));
