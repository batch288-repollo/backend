const User = require("../models/Users.js");
const Course = require("../models/Courses.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

module.exports.registerUser = (request, response) => {
	User.findOne({email: request.body.email}).then(result => {
		if (result) {
			return response.send(`Email ${request.body.email} has been taken! Try logging in or use different email in signing up!`);
		}
		else {
			new User({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo
			}).save().then(saved => {
				return response.send(`${request.body.email} is now registered!`);
			}).catch(error => response.send(error));
		}
	}).catch(error => response.send(error));
}

module.exports.loginUser = (request, response) => {
	User.findOne({email: request.body.email}).then(result => {
		if (result) {
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			if (isPasswordCorrect) {
				return response.send({auth: auth.createAccessToken(result)});
			}
			else {
				return response.send("Please check your Password!");
			}
		}
		else {
			return response.send(`Email ${request.body.email} is not yet registered!`);
		}
	}).catch(error => response.send(error));
}

module.exports.userDetails = (request, response) => {
	User.findOne({_id: request.body.id}).then(result => {
		if (result) {
			let resultModified = result;
			resultModified.password = '';
			return response.send(resultModified);
		}
		else {
			return response.send(`User ID ${request.body.id} is not yet registered!`);
		}
	}).catch(error => response.send(error));
}

module.exports.enrollCourse = async (request, response) => {
	const courseId = request.body.courseId;
	const userData = auth.decode(request.headers.authorization);
	let isAlreadyEnrolledCourse = false;
	let isAlreadyEnrolledUser = false;
	let isCourseInactive = false;
	let i = 0;

	if (userData.isAdmin) {
		return response.send("Admin cannot enroll a course!");
	}
	else {
		// Push update to "users" document array objects.
		let isUserUpdated = await User.findOne({_id: userData.id}).then(result => {			
			for (i = 0; i < result.enrollments.length; i++) {
				if (result.enrollments[i].courseId == courseId) {
					isAlreadyEnrolledCourse = true;
					return false;
				}
			}
			if (i === result.enrollments.length) {
				result.enrollments.push({
					courseId: courseId
				});
				return result.save().then(saved => true).catch(error => {
					console.log(error);
					return false;
				});
			}
		}).catch(error => {
			console.log(error);
			return false;
		});
		// Push update to "courses" document array objects.
		let isCourseUpdated = await Course.findOne({_id: courseId}).then(result => {
			if (result.isActive == false) {
				isCourseInactive = true;
				return false;
			}

			for (i = 0; i < result.enrollees.length; i++) {
				// console.log(result.enrollees[i].userId)
				if (result.enrollees[i].userId == userData.id) {
					isAlreadyEnrolledUser = true;
					return false;
				}
			}

			if (i === result.enrollees.length) {
				result.enrollees.push({
					userId: userData.id
				});
				return result.save().then(saved => true).catch(error => {
					console.log(error);
					return false;
				});
			}
		}).catch(error => {
			console.log(error);
			return false;
		});

		// If condition to check whether we updated users and courses document successfully.
		if (isAlreadyEnrolledCourse && isAlreadyEnrolledUser) {
			return response.send("You are already enrolled to this course!");
		}
		else if (isUserUpdated && isCourseUpdated) {
			return response.send("Enrollment is successful!");
		}
		else if (isCourseInactive) {
			return response.send("Course inactive, you cannot enroll this course!");
		}
		else {
			return response.send("There was an error in the enrollment please try again!");
		}
	}
}
