const Course = require("../models/Courses.js");
const auth = require("../auth.js");

module.exports.addCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		new Course({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive,
			slots: request.body.slots
		}).save().then(saved => {
			return response.send(`Course ${request.body.name} was successfully created!`);
		}).catch(error => response.send(error));
	}
	else {
		return response.send("You are not an admin, you don't have access to this route.");
	}
}

module.exports.getallCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		Course.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
	else {
		return response.send("You don't have access to this route.");
	}
}

module.exports.getActiveCourses = (request, response) => {
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

module.exports.getCourse = (request, response) => {
	Course.findById(request.params.courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

module.exports.updateCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		const courseId = request.params.courseId;
		let updatedCourse = {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price
		}
		Course.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(`Course ${courseId} successfully updated.`))
		.catch(error => response.send(error));
	}
	else {
		return response.send("You don't have access to this route.");
	}
}

module.exports.archiveCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		const courseId = request.params.courseId;
		const updatedCourse = {
			isActive: request.body.isActive
		}
		Course.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(`Course ${courseId} successfully archived.`))
		.catch(error => response.send(error));
	}
	else {
		return response.send("You don't have access to this route.");
	}
}

module.exports.getInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if (userData.isAdmin) {
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
}
