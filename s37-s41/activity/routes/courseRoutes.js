const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");
const express = require("express");

const router = new express.Router();
router.post("/addCourse", auth.verify, courseControllers.addCourse);
router.get("/", auth.verify, courseControllers.getallCourses);
router.get("/activeCourses", courseControllers.getActiveCourses);
router.get("/inactiveCourses", auth.verify, courseControllers.getInactiveCourses);
router.get("/:courseId", courseControllers.getCourse);
router.patch("/:courseId", auth.verify, courseControllers.updateCourse);
router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourse);

module.exports = router;
