const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js")
const express = require("express");
const router = express.Router();

// Routes:
router.post("/register", userControllers.registerUser);
router.get("/login", userControllers.loginUser);
router.post("/details", auth.verify, userControllers.userDetails);
router.post("/enroll", auth.verify, userControllers.enrollCourse);

module.exports = router;
