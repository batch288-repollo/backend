console.log("---------------------------------------------------------------------------------------");

// [Section] Exponent Operator;

// Before the ES6 update:
// Math.pow(base, exponent);
const firstNum = Math.pow(8, 2);
console.log(firstNum);
const secondNum = Math.pow(5, 5);
console.log(secondNum);

// After the ES6 update:
console.log("=======================================");
const thirdNum = 8 ** 2;
console.log(thirdNum);
const fourthNum = 5 ** 5;
console.log(fourthNum);

// [Section] Template Literals
// It will allow us to write strings without using the concatenation operator.
// Greatly helps with the code readability.

// Before ES6 Update:
let name = 'John';
let message = 'Hello ' + name + '! Welcome to programming!';
console.log("message");

// After the ES6 Update:
// Uses backticks (``) / tilde.
message = `Hello ${name}! Welcome to programming!`;
console.log(message);
console.log(typeof message);

// Multi-line using Template literals.
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8**2 with the
solution of ${firstNum}`;

console.log(anotherMessage);

// Template literals allows us to write strings with embedded JavaScript expressions.
// Expressions are any valid unit of code that resolves to a value.
// '${expressions/variables}' are used to include JavaScript expressions in strings using the template literals.
const interetRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${interetRate * principal}`);


// [SECTION] Array Destructuring
/*
	- allows us to unpack elements in array into distinct variables BASED on ORDER of initialization.
	Syntax:
	let/const [variableNameA, variableNameB, variableNameC, ...] = arrayName;
*/
const fullName = ["Juan", "Dela", "Cruz"];

// Before ES6 Update:
let firstName = fullName[0];
let middleName = fullName[1];
let lastName = fullName[2];
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

// After the ES6 Update:
const [name1, name2, name3] = fullName;
console.log(name1);
console.log(name2);
console.log(name3);
// This cause error!
// name1 = 'error';

// Another example of Array Destructuring:
let gradesPerQuarter = [98, 97, 95, 94];
console.log(gradesPerQuarter);
let [firstGrading, secondGrading, thirdGrading, fourthGrading] = gradesPerQuarter;
firstGrading = 100;
console.log(firstGrading);
console.log(secondGrading);
console.log(thirdGrading);
console.log(fourthGrading);


// [Section] Object Destructuring
// Allow us to unpack properties of objects into distinct variables BASED on PROPERTY/VARIABLE NAME(s).
// Shortens the syntax for accessing the properties from objects.
/*
	Syntax:
	let/const {propertyNameA, propertyNameB, ...} = objectName;
*/
// Before the ES6 update:
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};
console.log(person);
// const givenName = person.givenName;
// const maidenName = person.maidenName;
// const familyName = person.familyName;
// console.log(givenName);
// console.log(maidenName);
// console.log(familyName);

// After the ES6 Update:
const {givenName, familyName, maidenName} = person;
console.log(`This is the givenName: ${givenName}`);
console.log(`This is the maidenName: ${maidenName}`);
console.log(`This is the familyName: ${familyName}`);


// [SECTION] Arrow Function
// Compact alternative syntax to a traditional function/s.
/*
	Syntax:
	const/let keyword variableName = () => {
		statement/codeblock;
	}
*/
console.log("---------------------------------------------------------------------------------------");
// Arrow function without parameter.
const hello = () => {
	console.log("Hello World from the arrow function.");
}
hello();

// Arrow function with parater/s.
/*
	Syntax:
	const/let keyword variableName = (parameter) => {
		statement/codeblock;
	}
*/
console.log("---------------------------------------------------------------------------------------");
const printfullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial}. ${lastName}`);
};
printfullName("John", "D", "Smith");

// Arrow function can also be used with loops.
// Example:
const students = ["John", "Jane", "Judy"];
students.forEach((student) => {
	console.log(`${student} is a student.`)
});


// [SECTION] Implicit return in Arrow function.
// Example:
// If the function will run one line or one statement, the arrow function will implicitly return the value.
const add = (x, y) => x + y;
let total = add(10, 12);
console.log(total);


// [SECTION] Default function Argument value.
// Provides a default function argument if none is provided when the function is invoked.
console.log("---------------------------------------------------------------------------------------");
const greet = (name = "User", age = 0) => {
	return `Good morning, ${name}! I am ${age} years old!`;
}
console.log(greet());
console.log(greet('BebeMon', 99));
console.log(greet(undefined, 99)); // If you want to skip parameters, put 'undefined'.

function addNumber(x = 0, y = 0) {
	console.log(x);
	console.log(y);
	return x + y;
}
console.log(addNumber(2));


// [SECTION] Class-Based Object Blueprints
// Allow us to create / instantiate objects using a class blueprint.
/*
	Syntax:

	class className {
		constructor(objectPropertyA, objectPropertyB) {
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB;
		}
	}
*/
class Car {
	constructor(brand = '', name = '', year = 2020) {
		this.brand = brand;
		this.name = name;
		this.year = year;

		this.drive = () => {
			console.log(`The car is moving 60 km per hour.`);
		}
	}
}

// Instantiate an Object:
// const myCar = new Car('Toyota', 'Supra', 2020);
const myCar = new Car('Ford', 'Ranger Raptor', 2021);
console.log(myCar);
myCar.drive();

const defaultCar = new Car();
console.log(defaultCar);

